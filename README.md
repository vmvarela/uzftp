# uzftp

Single-user FTP server that autounzips files on incoming.


## Usage

You use environment variables to change your settings.

	FTP_IPADDRESS=<external IP>
	FTP_USER=<username>
	FTP_PASS=<password>
	FTP_PATH=<path to incoming files>
	FTP_UNZIPPED=<path to archive unzipped files>

## Example

	$ sudo docker run -it --rm \
		-p 20:20 -p 21:21 \
		-p 12020:12020 -p 12021:12021 -p 12022:12022 -p 12023:12023 -p 12024:12024 \
		-e FTP_USER=myuser -e FTP_PASS=mypass -e FTP_UNZIPPED=/archive \
		near/uzftp
	
NOTE: Ports 20/tcp and 21/tcp must be redirected, and 12020-12025 to allow passive mode.

	
