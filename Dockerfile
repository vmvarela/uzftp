FROM		debian:jessie
MAINTAINER	Victor M. Varela <v.varela@neartechnologies.com>

RUN		apt-get update \
	&&	apt-get install -y --no-install-recommends python-pyftpdlib \
	&&	apt-get clean \
	&&	rm -rf /var/lib/apt/lists/*

RUN		mkdir /ftp
COPY		server.py /server.py

VOLUME		/ftp

ENV		FTP_IPADDRESS	0.0.0.0
ENV		FTP_USER	user
ENV		FTP_PASS	pass
ENV		FTP_PATH	/ftp
ENV		FTP_UNZIPPED	/ftp/unzipped

CMD		["python", "server.py"]

