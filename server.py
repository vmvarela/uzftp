#!/usr/bin/env python
#
# unzip_ftpd is a simple FTP server that unzip all incoming .ZIP files
# 
# Copyright (C) 2014-2015 Near Technologies
# Author: Victor M. Varela

from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.log import logger

import os, zipfile, shutil

class UnzipFTPHandler(FTPHandler):


    # auto-unzip .zip files
    def on_file_received(self, file):
       unzipDir = os.path.join(os.getenv('FTP_UNZIPPED', '/ftp/.unzipped/'))
       if not os.path.exists(unzipDir):
                os.mkdir(unzipDir)
       if zipfile.is_zipfile(file):
                zfile = zipfile.ZipFile(file)
                destDir = os.path.dirname(file)
                for name in zfile.namelist():
                        (dirName, fileName) = os.path.split(name)
                        if fileName == '':
                                # directory
                                newDir = destDir + '/' + dirName
                                if not os.path.exists(newDir):
                                        os.mkdir(newDir)
                        else:
                                # file
                                logger.info("FROM %s >>>  %s" % (file, name))
                                fd = open(destDir + '/' + name, 'wb')
                                fd.write(zfile.read(name))
                                fd.close()
                zfile.close()
                shutil.move(file, os.path.join(unzipDir, os.path.basename(file)))

def main():
    authorizer = DummyAuthorizer()
    authorizer.add_user(os.getenv('FTP_USER','user'), os.getenv('FTP_PASS','pass'), os.getenv('FTP_PATH','/ftp'), perm='elradfmwM')

    handler = UnzipFTPHandler
    handler.authorizer = authorizer
    handler.banner = "UZFTP ready."
    handler.masquerade_address = os.environ['FTP_IPADDRESS']
    handler.passive_ports = range(12020,12025)
    server = FTPServer(('', 21), handler)
    server.serve_forever()

if __name__ == "__main__":
    main()

